#include <bits/stdc++.h>

using namespace std;

//*** Subset Sum problem
// for the subset Sum problem the logic is the same

// Subset sum problem is the problem where we have a set of integers
// and we need to answer whether we can form a set, such that the sum will be equal to X
// where X is the given value for the problem

// the only difference we store boolean values and OR them with possible variations


//***Cutting rod problem
// We have a rod, and we know that different lenghts of a rod can be sold
// with different price as it is given as an input
// if (rod >= piece[i].length) {
// 	dp[i][rod] = max(dp[i - 1][rod], dp[i][rod - piece[i].length] + piece[i].price);
// } else {
// 	dp[i][rod] = dp[i - 1][rod];
// }

int findMaxProfit(const vector<pair<int, int> > items, int totalWeight) {
	int itemCount = items.size();
	vector<vector<int>> dp(itemCount, vector<int>(totalWeight + 1, 0));

	for (int index = 1; index < itemCount; index++) {
		for (int weight = 1; weight <= totalWeight; weight++) {
			if (weight >= items[index - 1].second) {
				dp[index][weight] = max(dp[index - 1][weight],
					dp[index - 1][weight - items[index - 1].second] + items[index - 1].first);
			} else {
				dp[index][weight] = dp[index - 1][weight];
			}
		}
	}

	return dp[itemCount - 1][totalWeight];
}


int main() {
	freopen("in", "r", stdin);
	freopen("out", "w", stdout);
	int totalWeight, itemCount;
	cin >> itemCount >> totalWeight;
	vector<pair<int, int> > items(itemCount);
	for (int index = 0; index < itemCount; index++) {
		cin >> items[index].first >> items[index].second; //price and weight
	}
	cout << findMaxProfit(items, totalWeight);

	return 0;
}
