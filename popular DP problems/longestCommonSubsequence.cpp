#include <bits/stdc++.h>

using namespace std;


//recursive O(2^n) approach
int LCSRecursive(const string& str1, const string& str2, int index1, int index2) {
	if (index1 == 0 || index2 == 0) {
		return 0;
	}
	if (str1[index1 - 1] == str2[index2 - 1]) {
		return 1 + LCSRecursive(str1, str2, index1 - 1, index2 - 1);
	}
	return max(LCSRecursive(str1, str2, index1 - 1, index2),
				LCSRecursive(str1, str2, index1, index2 - 1));
}

//DP approach
int longestCommonSubsequence(const string& str1, const string& str2) {
	vector<vector<int> > dp(str1.size() + 1, vector<int>(str2.size() + 1, 0));
	for (int i = 1; i <= str1.size(); i++) {
		for (int j = 1; j <= str2.size(); j++) {
			if (str1[i - 1] == str2[j - 1]) {
				dp[i][j] = dp[i - 1][j - 1] + 1;
			} else {
				dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
			}
		}
	}
	return dp[str1.size()][str2.size()];
}


int main() {
	freopen("in", "r", stdin);
	freopen("out", "w", stdout);
	string str1, str2;
	cin >> str1 >> str2;
	cout << longestCommonSubsequence(str1, str2) << endl;
	cout << LCSRecursive(str1, str2, str1.size(), str2.size());

	return 0;
}
