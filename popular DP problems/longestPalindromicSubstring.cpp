class Solution {
public:
    string longestPalindrome(string s) {
        if(s.empty()) {
            return "";
        }
        vector<vector<int> > dp(s.size(), vector<int>(s.size(), 0));
        for(int i = 0; i < dp.size(); i++) {
            for(int j = 0; j < dp[i].size(); j++) {
                if(i == j) {
                    dp[i][j] = 1;
                }
            }
        }
        int x = 0, y = 1;
        while(x < s.size() && y < s.size()) {
            if(s[x] == s[y]) {
                dp[x][y] = 2;
            }
            x++;
            y++;
        }
        y = 2;
        while(y < s.size()) {
            int curX = 0, curY = y;
            while(curX < s.size() && curY < s.size()) {
                if(s[curX] == s[curY] && dp[curX + 1][curY - 1] != 0) {
                    dp[curX][curY] = dp[curX + 1][curY - 1] + 2;
                }
                curX++;
                curY++;
            }
            y++;
        }
        x = 0; y = 0;
        int maxValue = -1;
        for(int i = 0; i < dp.size(); i++) {
            for(int j = 0; j < dp[i].size(); j++) {
                // cout << dp[i][j] << " ";
                if(dp[i][j] > maxValue) {
                    maxValue = dp[i][j];
                    x = i;
                    y = j;
                }
            }
        }

        return s.substr(x, y - x + 1);
    }
};
