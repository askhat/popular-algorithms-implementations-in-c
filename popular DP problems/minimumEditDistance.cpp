class Solution {
public:
    int minDistance(string word1, string word2) {
        vector<vector<int> > d(word1.size() + 1, vector<int>(word2.size() + 1, 0));
        for(int i = 0; i <= word1.size(); i++) {
            for(int j = 0; j <= word2.size(); j++) {
                if(i == 0) {
                    d[i][j] = j;
                } else if(j == 0) {
                    d[i][j] = i;
                } else if(word1[i - 1] == word2[j - 1]) {
                    d[i][j] = d[i - 1][j - 1];
                } else {
                    d[i][j] = min(min(d[i - 1][j], d[i][j - 1]), d[i - 1][j - 1]) + 1;
                }
            }
        }
        return d[word1.size()][word2.size()];
    }
};
