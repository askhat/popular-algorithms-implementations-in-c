#include <bits/stdc++.h>

using namespace std;

int minCoins(const vector<int>& coins, int amount) {
	//we have inifinte amount of coins for each denomination
	vector<long long> dp(amount + 1, 2e9);
	dp[0] = 0;
	for (const auto& coin : coins) {
		for (int val = 1; val <= amount; val++) {
			if (val >= coin) {
				dp[val] = min(dp[val], dp[val - coin] + 1);
			}
		}
	}
	return dp[amount];
}

int main() {
	freopen("in", "r", stdin);
	freopen("out", "w", stdout);
	int n, amount;
	cin >> n >> amount;
	vector<int> coins(n);
	for (int index = 0; index < n; index++) {
		cin >> coins[index];
	}
	cout << minCoins(coins, amount) << endl;

	return 0;
}
