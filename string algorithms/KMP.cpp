#include <iostream>
#include <string>
#include <vector>

#include <cmath>
#include <cstdio>

using namespace std;

vector<int> prefix(string str) {
    int n = str.size(), j = 0;
    vector<int> p(n);
    for(int i = 1; i < n; i++) {
        while(str[i] != str[j] && j > 0) {
            j = p[j - 1];
        }
        if(str[i] == str[j]) {
            j++;
        }
        p[i] = j;
    }
    return p;
}


void KMP(string str, string pat) {
    int n = str.size(), m = pat.size(), i = 0, j = 0;
    vector<int> p = prefix(pat);

    while(i < n) {
        if(str[i] == pat[j]) {
            i++;
            j++;
        }
        if(j == m) {
            cout << i - j << endl;
            j = p[j - 1]; //overlap, j = 0 otherwise
        }

        if(str[i] != pat[j]) {
            if(j > 0) {
                j = p[j - 1];
            } else {
                i++;
            }
        }
    }
}

int main(int argc, char const *argv[]) {
    freopen("in", "r", stdin);
    freopen("out", "w", stdout);
    string str, pat;
    cin >> str >> pat;
    KMP(str, pat);

    return 0;
}
