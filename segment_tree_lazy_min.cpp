#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 65;
int segTree[4 * N], a[N], lazy[4 * N];

void build(int low, int high, int pos) {
 	if (low == high) {
 		segTree[pos] = a[low];
 		return;
 	}
 	int mid = (low + high) / 2;
 	build(low, mid, 2 * pos + 1);
 	build(mid + 1, high, 2 * pos + 2);
 	segTree[pos] = min(segTree[2 * pos + 1], segTree[2 * pos + 2]);
}

int RMQLazy(int qlow, int qhigh, int low, int high, int pos) {
    if (lazy[pos] != 0) {
    	segTree[pos] += lazy[pos];
    	if (low != high) {
    		lazy[2 * pos + 1] += lazy[pos];
    		lazy[2 * pos + 2] += lazy[pos];
    	}
    	lazy[pos] = 0;
    }
	if (qlow > high || qhigh < low) return INT_MAX;
	if (qlow <= low && qhigh >= high) return segTree[pos];
	int mid = (low + high) / 2;
	int left = RMQLazy(qlow, qhigh, low, mid, 2 * pos + 1);
	int right = RMQLazy(qlow, qhigh, mid + 1, high, 2 * pos + 2);
	return min(left, right);
}

void updateLazy(int qlow, int qhigh, int delta, int low, int high, int pos) {
	if (lazy[pos] != 0) {
		segTree[pos] += lazy[pos];
    	if (low != high) {
    		lazy[2 * pos + 1] += lazy[pos];
    		lazy[2 * pos + 2] += lazy[pos];
    	}
    	lazy[pos] = 0;		
	}
	if (qlow > high || qhigh < low) return;
	if (qlow <= high && qhigh >= high) {
		segTree[pos] += delta;
		if (low != high) {
			lazy[2 * pos + 1] += delta;
			lazy[2 * pos + 2] += delta;	
		}
		return;
	}
	int mid = (low + high) / 2;
	updateLazy(qlow, qhigh, delta, low, mid, 2 * pos + 1);
	updateLazy(qlow, qhigh, delta, mid + 1, high, 2 * pos + 2);
	segTree[pos] = min(segTree[2 * pos + 1], segTree[2 * pos + 2]);
}


int main() {
	freopen("in", "r", stdin);
	freopen("out", "w", stdout);
	int n, q, l, r;
	cin >> n;
	for (int i = 0 ;i < n; i++) cin >>a[i];
	cin >> q;
	build(0, n - 1, 0);
	updateLazy(0, 3, -100, 0, n - 1, 0);


	while (q--) {
		cin >> l >> r;
		cout << RMQLazy(l, r, 0, n - 1, 0) << endl;
	}	                        	

	return 0;
}
