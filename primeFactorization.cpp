#include <bits/stdc++.h>

using namespace std;


//prime factorization in O(sqrt(n))
vector<pair<int, int> > primeFactorization(int number) {
	vector<pair<int, int> > result;
	for (int factor = 2; factor <= sqrt(number); factor++) {
		if (number % factor == 0) {
			int counter = 0;
			while (number % factor == 0) {
				number /= factor;
				counter++;
			}
			result.push_back({factor, counter});
		}
	}
	if (number != 1) {
		result.push_back({number, 1});
	}
	return result;
}

int main() {
	freopen("in", "r", stdin);
	freopen("out", "w", stdout);
	int number;
	cin >> number;
	auto primeFactors  = primeFactorization(number);
	for (const auto& primeFactor : primeFactors) {
		cout << primeFactor.first << " " << primeFactor.second << endl;
	}

	return 0;
}
