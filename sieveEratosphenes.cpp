#include <iostream>
#include <string>
#include <cstdio>
#include <vector>
#include <cmath>


using namespace std;

const int N = 1e5 + 65;
int sieve[N];


void sieveOfEratosphenes(int n) {
	sieve[0] = 1;
	sieve[1] = 1;
	for(int i = 2; i <= sqrt(n); i++) {
		if(sieve[i] == 0) {
			for(int j = 2 * i; j <= n; j += i) {
				sieve[j] = 1;
			}
		}
	}
}


int main() {
	freopen("in", "r", stdin);
	freopen("out", "w", stdout);
	int n, q, x;
	cin >> n >> q;
	sieveOfEratosphenes(n);
	while(q--) {
		cin >> x;
		if(!sieve[x]) {
			cout << "PRIME " << endl;
		} else {
			cout << "NOT PRIME " << endl;
		}
	}


	return 0;
}
