#include <bits/stdc++.h>
using namespace std;
const int N = 1e5 + 65;
int a[N], d[N][(int)log2(N) + 1];

void preprocess(int n) {
    for (int i = 0; i < n; i++) d[i][0] = a[i];
    for (int j = 1; j < (int)log2(n) + 1; j++) {
    	for (int i = 0; i + (1 << j) - 1 < n; i++) {
    		d[i][j] = d[i][j - 1] + d[i + (1 << (j - 1))][j - 1];
    	}
    }
}

int RMQ(int l, int r) {
	int ans = 0;
	for (int k = 16; k >= 0; k--) {
		if (l + (1 << k ) - 1 <= r) {
			ans += d[l][k];
			l += (1 << k);
		}
	}
	return ans;
}

int RMQMax(int l, int r) {
	int len = r - l + 1;
	int k = (int)log2(len);
	return max(d[l][k], d[r - (1 << k) + 1][k]);
}

int main() {
	freopen("in", "r", stdin);
	freopen("out", "w", stdout);
	int n, l, r, q;
	cin >> n;
	for (int i = 0 ;i < n ;i++) cin >> a[i];
	cin >> q;
	preprocess(n);
	while (q--) {
		cin >> l >> r;
		cout << RMQ(l, r) << endl;
	}
	return 0;
}
