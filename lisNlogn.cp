class Solution {
public:
    int lengthOfLIS(vector<int>& nums) {
        if(nums.size() == 1) return 1;
        set<int> curSet;
        for(int i = 0; i < nums.size(); i++) {
            curSet.insert(nums[i]);
            auto it = curSet.find(nums[i]);
            it++;
            if(it != curSet.end()) {
                curSet.erase(it);
            }
        }
        if(curSet.size() == 1 && nums[0] == 1) return 2;
        return curSet.size();
    }
};
