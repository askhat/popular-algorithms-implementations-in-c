#include <iostream>
#include <string>
#include <cstdio>
#include <vector>

using namespace std;

int gcd(int a, int b, int* x, int* y) {
	if(a == 0) {
		*x = 0;
		*y = 1;
		return b;	
	}
	int x1, y1;

	int res = gcd(b % a, a, &x1, &y1);

	*x = y1 - (b / a) * x1;
	*y = x1;

	return res;
}

int modularInverse(int a, int m) {
	int x, y;
	int g = gcd(a, m, &x, &y);
	if(g != 1) {
		return -1;
	} 
	int res = (x % m + m) % m;
	
	return res;	
}

int modularDivision(int a, int b, int mod) {
	// (a / b) % m = ((a % m) * (b ^ -1 % m)) % m
	int inverseOfB = modularInverse(b, mod);
	return ((a % mod) * (inverseOfB % mod)) % mod;
}



int main() {
	freopen("in", "r", stdin);	
	freopen("out", "w", stdout);
	int a, b, mod;
	cin >> a >> b >> mod;
	cout << modularDivision(a, b, mod);
	
	return 0;
}
