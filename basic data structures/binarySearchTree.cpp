#include <iostream>
#include <cstdio>

using namespace std;

struct Node {
	Node* left;
	Node* right;
	int data;
};

Node* root;

Node* getNewNode(int data) {
	Node* temp = new Node;
	temp -> data = data;
	temp -> left = NULL;
	temp -> right = NULL;
	return temp;
}

//PreOrder
void DFS(Node* cur) {
	if(cur == NULL) return;
	cout << cur -> data << " ";
	if(cur -> left != NULL) DFS(cur -> left);
	if(cur -> right != NULL) DFS(cur -> right);
}

void insert(int data, Node* cur) {
	if(root == NULL) {
		root = getNewNode(data);
		return;
	}
	if(data <= cur -> data) {
		if(cur -> left != NULL) {
			insert(data, cur -> left);
		} else {
			cur -> left = getNewNode(data);
		}
	} else {
		if(cur -> right != NULL) {
			insert(data, cur -> right);
		} else {
			cur -> right = getNewNode(data);
		}
	}
}

bool search(Node* cur, int data) {
	if(cur == NULL) return false;

	if(cur -> data == data) return true;
	else if(data <= cur -> data) return search(cur -> left, data);
	else return search(cur -> right, data);
}

Node* FindMin(Node* root) {
	while(root->left != NULL) root = root->left;
	return root;
}

// Function to search a delete a value from tree.
Node* Delete(Node *root, int data) {
	if(root == NULL) return root;
	else if(data < root->data) root->left = Delete(root->left,data);
	else if (data > root->data) root->right = Delete(root->right,data);
	// Wohoo... I found you, Get ready to be deleted
	else {
		// Case 1:  No child
		if(root->left == NULL && root->right == NULL) {
			delete root;
			root = NULL;
		}
		//Case 2: One child
		else if(root->left == NULL) {
			Node *temp = root;
			root = root->right;
			delete temp;
		}
		else if(root->right == NULL) {
			Node *temp = root;
			root = root->left;
			delete temp;
		}
		// case 3: 2 children
		else {
		 	Node *temp = FindMin(root->right);
			root->data = temp->data;
			root->right = Delete(root->right,temp->data);
		}
	}
	return root;
}

//find a height of a tree
int findHeight(Node* root) {
	if(root == NULL) return -1;
	int leftHeight = findHeight(root -> left);
	int rightHeight = findHeight(root -> right);
	return max(leftHeight, rightHeight) + 1;
}




/*vector<int> Solution::postorderTraversal(TreeNode* A) {
    stack<TreeNode*> s1, s2;
    TreeNode* root = A;
    s1.push(root);
    while(!s1.empty()) {
        root = s1.top();
        s1.pop();
        s2.push(root);
        if(root -> left != NULL) {
            s1.push(root -> left);
        }
        if(root -> right != NULL) {
            s1.push(root -> right);
        }
    }
    vector<int> res;
    while(!s2.empty()) {
        TreeNode* cur = s2.top();
        res.push_back(cur -> val);
        s2.pop();
    }
    return res;

}

vector<int> Solution::inorderTraversal(TreeNode* A) {
    vector<int> res;
    stack<TreeNode*> curStack;
    TreeNode* root = A;
    while(true) {
        if(root != NULL) {
            curStack.push(root);
            root = root -> left;
        } else {
            if(curStack.empty()) break;
            TreeNode* temp = curStack.top();
            res.push_back(temp -> val);
            curStack.pop();
            root = temp -> right;
        }
    }
    return res;

}*/

int main() {
	freopen("in", "r", stdin);
	freopen("out", "w", stdout);
	root = NULL;
	int n, x, q;
	cin >> n;
	for(int i = 0; i < n; i++) {
		cin >> x;
		insert(x, root);
	}
	DFS(root);
	cout << endl;
	cin >> q;
	while(q--) {
		cin >> x;
		cout << search(root, x) << endl;
	}

	return 0;
}
