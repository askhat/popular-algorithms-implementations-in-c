#include <iostream>
#include <cstdio>
#include <string>

using namespace std;

struct Node {
	int data;
	Node* next;
};

Node* head;

void insertBegin(int data) {
	Node* temp = new Node;
	temp -> data = data;
	temp -> next = head;
	head = temp;	
}

void insertEnd(int data) {
	Node* temp = new Node;
	temp -> data = data;
	temp -> next = NULL;
		
	if(head == NULL) {
		head = temp;
		return;
	}

	Node* cur = head;
	while(cur -> next != NULL) {
		cur = cur -> next;
	}	
	cur -> next = temp;	
}

void insertAt(int data, int index) {
	if(index == 1) {
		insertBegin(data);
		return;
	}
	Node* toAdd = new Node;
	toAdd -> data = data;
	toAdd -> next = NULL;
	
	Node* cur = head;
	for(int i = 0; i < index - 2; i++) {
		cur = cur -> next;
	}

	toAdd -> next = cur -> next;
	cur -> next = toAdd;	

}	

void deleteAt(int index) {
	Node* cur = head;
	if(index == 1) {
		head = cur -> next;
		delete head;		
		return;	
	}
	for(int i = 0; i < index - 2; i++) {
		cur = cur -> next;
	}
	Node* toDelete = cur -> next;
	cur -> next = toDelete -> next;
	
	//free
	delete toDelete;
}

void printLinkedList() {
	Node* cur = head;
	cout << "List is: " << endl;
	while(cur != NULL) {
		cout << cur -> data << " ";
		cur = cur -> next;
	}	
	cout << endl;
}

int main() {
	freopen("in", "r", stdin);
	freopen("out", "w", stdout);
	head = NULL;
	int n, x, deleteIndex;
	cin >> n;
	for(int i = 0; i < n; i++) {
		cin >> x;
		insertEnd(x);
		printLinkedList();
	}	
	cin >> deleteIndex;
	deleteAt(deleteIndex);
	printLinkedList();
	



	return 0;
}
