#include <iostream>
#include <cstdio>
#include <string>

using namespace std;

struct Node {
	int data;
	Node* next;
};

void insertBegin(int data, Node** pointerToHead) {
	Node* temp = new Node;
	temp -> data = data;
	temp -> next = *pointerToHead;
	*pointerToHead = temp;	
}

void insertEnd(int data, Node** pointerToHead) {
	Node* temp = new Node;
	temp -> data = data;
	temp -> next = NULL;
		
	if(*pointerToHead == NULL) {
		*pointerToHead = temp;
		return;
	}

	Node* cur = *pointerToHead;
	while(cur -> next != NULL) {
		cur = cur -> next;
	}	
	cur -> next = temp;	
}

void insertAt(int data, int index, Node* head) {
	if(index == 1) {
		insertBegin(data, &head);
		return;
	}
	Node* toAdd = new Node;
	toAdd -> data = data;
	toAdd -> next = NULL;
	
	Node* cur = head;
	for(int i = 0; i < index - 2; i++) {
		cur = cur -> next;
	}

	toAdd -> next = cur -> next;
	cur -> next = toAdd;	

}	

void deleteAt(int index, Node** pointerToHead) {
	Node* cur = *pointerToHead;
	if(index == 1) {
		*pointerToHead = cur -> next;
		delete *pointerToHead;		
		return;	
	}
	for(int i = 0; i < index - 2; i++) {
		cur = cur -> next;
	}
	Node* toDelete = cur -> next;
	cur -> next = toDelete -> next;
	
	//free
	delete toDelete;
}

void reverseLinkedList(Node** pointerToHead) { //iteratively
	cout << "reversing... " << endl;
	Node* cur, *prev, *nextNode;
	prev = NULL;
	cur = *pointerToHead;
	while(cur != NULL) {
		nextNode = cur -> next;
		cur -> next = prev;
		prev = cur;
		cur = nextNode;
	}	
	*pointerToHead = prev;
}

void reverseRecV1(Node* cur, Node* prev, Node** pointerToHead) {
	if(cur == NULL) return;
	reverseRecV1(cur -> next, cur, pointerToHead);
	
	if(cur -> next == NULL) {
		*pointerToHead = cur;	
	}

	cur -> next = prev;
}

void reverseVersion2(Node* cur, Node** pointerToHead) {
	if(cur -> next == NULL) {
		*pointerToHead = cur;
		return;
	}	
	reverseVersion2(cur -> next, pointerToHead);
	(cur -> next) -> next = cur;
	cur -> next = NULL;
}



void printReverseRec(Node* cur) {
	if(cur == NULL) return;
	printReverseRec(cur -> next);	
	cout << cur -> data << " ";
}


void printLinkedList(Node* head) {
	Node* cur = head;
	cout << "List is: " << endl;
	while(cur != NULL) {
		cout << cur -> data << " ";
		cur = cur -> next;
	}	
	cout << endl;
}

int main() {
	freopen("in", "r", stdin);
	freopen("out", "w", stdout);
	Node* head = NULL;

	int n, x;
	cin >> n;
	for(int i = 0; i < n; i++) {
		cin >> x;
		insertEnd(x, &head);
	}	

	cout << "Original List " << endl;
	printLinkedList(head);
	cout << "Reversing v1... " << endl;
	reverseRecV1(head, NULL, &head);
	cout << "After reversing " << endl;
	printLinkedList(head);
	cout << "Reversing v2..." << endl;
	reverseVersion2(head, &head);
	printLinkedList(head);
	cout << "Printing in reverse order " << endl;
	printReverseRec(head);	

	return 0;
}
