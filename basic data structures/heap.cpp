#include <iostream>
#include <string>
#include <cstdio>

using namespace std;
int size = 0;
const int N = 1e5 + 65;
int arr[N];


void mySwap(int x, int y) {
    int temp = arr[x];
    arr[x] = arr[y];
    arr[y] = temp;
}

void heapifyUp() {
    int index = size - 1;
    while((index - 1) / 2 >= 0 && arr[(index - 1) / 2] > arr[index]) {
        mySwap((index - 1) / 2, index);
        index = (index - 1) / 2;
    }
}

void heapifyDown() {
    int index = 0;
    while(2 * index + 1 < size) {
        int smallIndex = 2 * index + 1;
        if(2 * index + 2 < size && arr[2 * index + 2] < arr[smallIndex]) {
            smallIndex = 2 * index + 2;
        }

        if(arr[index] < arr[smallIndex]) {
            break;
        } else {
            mySwap(index, smallIndex);
            index = smallIndex;
        }
    }
}

int peek() {
    int item = arr[0];
    arr[0] = arr[size - 1];

    size--;
    heapifyDown();
    return item;
}

void insert(int item) {
    arr[size] = item;
    size++;
    heapifyUp();
}


int main(int argc, char const *argv[]) {
    freopen("in", "r", stdin);
    freopen("out", "w", stdout);
    int n, x;
    cin >> n;
    for (size_t i = 0; i < n; i++) {
        cin >> x;
        insert(x);
    }
    for (size_t i = 0; i < size; i++) {
        cout << arr[i] << " ";
    }
    cout << endl;
    for (size_t i = 0; i < n; i++) {
        cout << peek() << " ";
        // for (size_t i = 0; i < size; i++) {
        //     cout << arr[i] << " ";
        // }
        // cout << endl;
    }

    return 0;
}
