#include <iostream>
#include <string>
#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

struct Node {
	int data;
	Node* next;
};
Node* front, *end;

bool isEmpty() {
	return (front == NULL && end == NULL);
}

void enqueue(int data) {
	Node* temp = new Node;
	temp -> data = data;
	temp -> next = NULL;

	if(isEmpty()) {
		front = temp;
		end = temp;
		return;
	}

	end -> next = temp;
	end = temp;
}

void dequeue() {
	Node* temp = front;
	if(front == NULL) return;
	if(front == end) {
		front = NULL;
		end = NULL;
	} else {
		front = front -> next;
	}
	delete temp;
}

int peek() {
	return (front -> data);
}


int main() {
	freopen("in", "r", stdin);
	freopen("out", "w", stdout);
	front = NULL, end = NULL;
	int n, x;
	cin >> n;
	while(n--) {
		cin >> x;
		enqueue(x);
	}


	while(!isEmpty()) {
		cout << peek() << endl;
		dequeue();
	}




	return 0;
}
