#include <iostream>
#include <string>

using namespace std;
const int N = 1e5 + 65;
int a[N], segTree[4 * N], lazy[4 * N];

void build(int low, int high, int pos) {
    if (low == high) {segTree[pos] = a[low];return;}
    int mid = (low + high) / 2;
    build(low, mid, 2 * pos + 1);
    build(mid + 1, high, 2 * pos + 2);
    segTree[pos] = segTree[2 * pos + 1] + segTree[2 * pos + 2];
}

int RMQLazy(int low, int high, int qlow, int qhigh, int pos) {
    if (lazy[pos] != 0) {
        segTree[pos] += (high - low + 1) * lazy[pos];
        if (low != high) {
            lazy[2 * pos + 1] += lazy[pos];
            lazy[2 * pos + 2] += lazy[pos];
        }
        lazy[pos] = 0;
    }
    if (qlow > high || qhigh < low) return 0;
    if (qlow <= low && qhigh >= high) return segTree[pos];

    int mid = (low + high) / 2;
    int left = RMQLazy(low, mid, qlow, qhigh, 2 * pos + 1);
    int right = RMQLazy(mid + 1, high, qlow, qhigh, 2 * pos + 2);
    return left + right;
}

void updateLazy(int qlow, int qhigh, int delta, int low, int high, int pos) {
    if (lazy[pos] != 0) {
        segTree[pos] += (high - low + 1) * lazy[pos];
        if (low != high) {
            lazy[2 * pos + 1] += lazy[pos];
            lazy[2 * pos + 2] += lazy[pos];
        }
        lazy[pos] = 0;
    }
    if (qlow > high || qhigh < low) return;
    if (qlow <= low && qhigh >= high) {
        segTree[pos] += (high - low + 1) * delta;
        if (low != high) {
            lazy[2 * pos + 1] += delta;
            lazy[2 * pos + 2] += delta;
        }
        return;
    }
    int mid = (low + high) / 2;
	updateLazy(qlow, qhigh, delta, low, mid, 2 * pos + 1);
	updateLazy(qlow, qhigh, delta, mid + 1, high, 2 * pos + 2);
    segTree[pos] = segTree[2 * pos + 1] + segTree[2 * pos + 2];
}


int main(int argc, char const *argv[]) {
	freopen("in", "r", stdin);
	freopen("out", "w", stdout);
	int n, q, l, r;
    cin >> n;
    for (int i = 0; i < n; i++) cin >> a[i];
    cin >> q;
    build(0, n - 1, 0);
    updateLazy(0, n - 1, 10, 0, n - 1, 0);
    while (q--) {
        cin >> l >> r;
        cout << RMQLazy(0, n - 1, l, r , 0) << endl;
    }

	return 0;
}
