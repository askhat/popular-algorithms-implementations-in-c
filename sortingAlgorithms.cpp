#include <iostream>
#include <string>
#include <cstdio>
#include <vector>



using namespace std;

void mySwap(int* a, int* b) {
	int temp = *a;
	*a = *b;
	*b = temp;
}


void selectionSort(int* a, int n) {
	int mn = 2e9, index = 0, curIndex = 0;
	while(index < n) {
		for(int i = index; i < n; i++) {
			if(a[i] < mn) {
				mn = a[i];
				curIndex = i;
			}
		}
		mn = 2e9;
		mySwap(&a[curIndex], &a[index]);
		index++;
	}
}
void bubbleSort(int* a, int n) {
	for(int i = 0; i < n - 1; i++) {
		for(int j = 0; j < n - 1; j++) {
			if(a[j + 1] < a[j]) {
				mySwap(&a[j + 1], &a[j]);
			}
		}
	}
}

void insertionSort(int* a, int n) {
	for(int i = 1; i < n; i++) {
		int curValue = a[i];
		int j = i;
		while(j > 0 && a[j - 1] > curValue) {
			a[j] = a[j - 1];
			j--;
		}
		a[j] = curValue;

	}
}

void show(int* a, int n) {
	for(int i = 0; i < n; i++) {
		cout << a[i] << " ";
	}
	cout << endl;
}

int partition(vector<int>& numbers, int left, int right) {
	int pivot = numbers[right], pIndex = left;
	for (int index = left; index < right; ++index) {
		if (numbers[index] <= pivot) swap(numbers[index], numbers[pIndex++]);
	}
	swap(numbers[pIndex], numbers[right]);
	return pIndex;
}

void quickSort(vector<int>& numbers, int left, int right) {
	if (left >= right) return;
	int pivot = partition(numbers, left, right);
	quickSort(numbers, left, pivot - 1);
	quickSort(numbers, pivot + 1, right);
}

void merge(const vector<int>& left, const vector<int>& right, vector<int>& numbers) {
	int l = 0, r = 0, index = 0;
	while (l < left.size() && r < right.size()) {
		if (left[l] <= right[r]) numbers[index++] = left[l++];
		else numbers[index++] = right[r++];
	}
	while (l < left.size()) numbers[index++] = left[l++];
	while (r < right.size()) numbers[index++] = right[r++];
}

void mergeSort(vector<int>& numbers) {
	if (numbers.size() < 2) return;
	int n = numbers.size(), mid = n / 2;

	vector<int> left(mid), right(numbers.size() - mid);
	for (int index = 0; index < mid; ++index) left[index] = numbers[index];
	for (int index = mid; index < n; ++index) right[index - mid] = numbers[index];
	mergeSort(left);
	mergeSort(right);
	merge(left, right, numbers);
}

int main() {
    freopen("in", "r", stdin);
    freopen("out", "w", stdout);
	int n;
	cin >> n;
	int a[n];
	for(int i = 0; i < n; i++) {
		cin >> a[i];
	}

	int partIndex = partitionArray(a, 0, n - 1);
	cout << "partIndex " << partIndex << endl;
	show(a, n);

	cout << "starting QuickSort" << endl;
	quickSort(a, 0, n - 1);
	show(a, n);




    return 0;
}
