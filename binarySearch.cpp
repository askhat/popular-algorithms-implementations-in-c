#include <iostream>
#include <cstdio>
#include <string>
#include <vector>

using namespace std;


int binaryIter(int* a, int n, int x) {
	int left = 0, right = n - 1;
	while(left <= right) {
		int mid = left + (right - left) / 2;
		if(a[mid] == x) return mid;
		else if(x < a[mid]) right = mid - 1;
		else left = mid + 1;
	}
	return -1;
}

int binaryRec(int* a, int left, int right, int x) {

	if(left > right) return -1;

	int mid = left + (right - left) / 2;

	if(a[mid] == x) {
		return mid;
	} else if(x < a[mid]) {
		binaryRec(a, left, mid - 1, x);
	} else {
		binaryRec(a, mid + 1, right, x);
	}
}


int main() {
	freopen("in", "r", stdin);
	freopen("out", "w", stdout);
	int n, x;
	cin >> n >> x;
	int a[n];
	for(int i = 0; i < n; i++) {
		cin >> a[i];
	}

	cout << binaryIter(a, n, x) << endl;
	cout << binaryRec(a, 0, n - 1, x) << endl;

	return 0;
}
