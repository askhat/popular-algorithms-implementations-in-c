#include <bits/stdc++.h>
using namespace std;

struct Node {
    bool isEnd;
    shared_ptr<Node> children[26];
};


shared_ptr<Node> getNewNode() {
    shared_ptr<Node> tmp(new Node);
    for (int i = 0; i < 26; ++i) {
        tmp->children[i] = nullptr;
    }
    tmp->isEnd = false;
    return tmp;
}

void insert(const string& str, shared_ptr<Node>& root) {
    if (root == nullptr) {
        root = getNewNode();
    }
    shared_ptr<Node> cur = root;
    for (const auto& ch : str) {
        int index = ch - 'a';
        if (cur->children[index] == nullptr) {
            cur->children[index] = getNewNode();
        }
        cur = cur->children[index];
    }
    cur->isEnd = true;
}

bool search(const string& str, const shared_ptr<Node>& root) {
    shared_ptr<Node> cur = root;
    for (const auto& ch : str) {
        int index = ch - 'a';
        if (cur->children[index] == nullptr) {
            return false;
        }
        cur = cur->children[index];
    }
    return cur->isEnd;
}



int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false); cin.tie(NULL);
    #ifndef ONLINE_JUDGE
    freopen("in", "r", stdin); freopen("out", "w", stdout);
    #endif

    int n, q;
    cin >> n;
    string str;

    shared_ptr<Node> root;

    for (size_t i = 0; i < n; i++) {
        cin >> str;
        insert(str, root);
    }

    cin >> q;
    while (q--) {
        cin >> str;
        if (search(str, root)) {
            cout << "YES" << endl;
        } else {
            cout << "NO" << endl;
        }
    }


    return 0;
}

