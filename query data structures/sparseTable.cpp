#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 65;
vector<vector<int> > dp(N, vector<int>((int)log2(N) + 1));

void preprocess(const vector<int>& numbers) {
	for (int index = 0; index < numbers.size(); ++index) dp[index][0] = numbers[index];
	for (int j = 1; j < (int)log2(numbers.size()) + 1; j++) {
		for (int i = 0; i + (1 << j) - 1 < numbers.size(); i++) {
			dp[i][j] = dp[i][j - 1] + dp[i + (1 << (j - 1))][j - 1];
		}
	}
}

int RMQSum(int l, int r) {
	int ans = 0;
	for (int k = 16; k >= 0; k--) {
		if (l + (1 << k) - 1 <= r) {
			ans += dp[l][k];
			l += (1 << k);
		}
	}
	return ans;
}

int RMQ(int l, int r) {
	int len = r - l + 1, k = (int)log2(len);
	return min(dp[l][k], dp[r - (1 << k) + 1][k]);
}

int main() {
	freopen("in", "r", stdin);
	freopen("out", "w", stdout);
	int n, q, l, r;
	cin >> n;
	vector<int> numbers(n);
	for (int index = 0; index < n; ++index) {
		cin >> numbers[index];
	}
	preprocess(numbers);
	cin >> q;
	while (q--) {
		cin >> l >> r;
		cout << RMQSum(l, r) << endl;
	}


	return 0;
}
