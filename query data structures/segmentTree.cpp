#include <iostream>
#include <string>

using namespace std;

const int N = 1e5 + 65, INF = 2e9;
int segTree[4 * N], a[N], lazy[4 * N];


void build(int low, int high, int pos) {
	if(low == high) {
		segTree[pos] = a[low];
		return;
	}
	int mid = (low + high) / 2;
	build(low, mid, 2 * pos + 1);
	build(mid + 1, high, 2 * pos + 2);
	segTree[pos] = min(segTree[2 * pos + 1], segTree[2 * pos + 2]);
}


int RMQ(int qlow, int qhigh, int low, int high, int pos) {
	if(qlow <= low && qhigh >= high) return segTree[pos];
	if(qlow > high || qhigh < low) return INF;
	int mid = (low + high) / 2;
	return min(RMQ(qlow, qhigh, low, mid, 2 * pos + 1), RMQ(qlow, qhigh, mid + 1, high, 2 * pos + 2));
}


void update(int index, int delta, int low, int high, int pos) {
	if(index < low || index > high) return;
	if(low == high) {
		segTree[pos] += delta;
		return;
	}
	int mid = (low + high) / 2;
	update(index, delta, low, mid, 2 * pos + 1);
	update(index, delta, mid + 1, high, 2 * pos + 2);

	segTree[pos] = min(segTree[2 * pos + 1], segTree[2 * pos + 2]);
}


int RMQLazy(int qlow, int qhigh, int low, int high, int pos) {
	if(lazy[pos] != 0) {
		segTree[pos] += lazy[pos];
		if(low != high) {
			lazy[2 * pos + 1] += lazy[pos];
			lazy[2 * pos + 2] += lazy[pos];
		}
		lazy[pos] = 0;
	}
	if(qlow > high || qhigh < low) return INF;
	if(qlow <= low && qhigh >= high) return segTree[pos];
	int mid = (low + high) / 2;

	return min(RMQ(qlow, qhigh, low, mid, 2 * pos + 1), RMQ(qlow, qhigh, mid + 1, high, 2 * pos + 2));
}


void updateLazy(int qlow, int qhigh, int delta, int low, int high, int pos) {
	if(lazy[pos] != 0) {
		segTree[pos] += lazy[pos];
		if(low != high) {
			lazy[2 * pos + 1] += lazy[pos];
			lazy[2 * pos + 2] += lazy[pos];
		}
		lazy[pos] = 0;
	}
	if(qlow > high || qhigh < low) return;

	if(qlow <= low && qhigh >= high) {
		segTree[pos] += delta;
		if(low != high) {
			lazy[2 * pos + 1] += delta;
			lazy[2 * pos + 2] += delta;
		}
		return;
	}
	int mid = (low + high) / 2;
	updateLazy(qlow, qhigh, delta, low, mid, 2 * pos + 1);
	updateLazy(qlow, qhigh, delta, mid + 1, high, 2 * pos + 2);

	segTree[pos] = min(segTree[2 * pos + 1], segTree[2 * pos + 2]);
}



int main(int argc, char const *argv[]) {
	freopen("in", "r", stdin);
	freopen("out", "w", stdout);
	int n, q, l, r, x, index, delta; char c;
	cin >> n;
	for (size_t i = 0; i < n; i++) {
		cin >> a[i];
	}
	build(0, n - 1, 0);
	cin >> q;
	for (size_t i = 0; i < q; i++) {
		cin >> c;
		if(c == 'Q') {
			cin >> l >> r;
			cout << RMQLazy(l, r, 0, n - 1, 0) << endl;
		} else {
			cin >> l >> r >> delta;
			updateLazy(l, r, delta, 0, n - 1, 0);
			cout << "updated" << endl;
		}
	}


	return 0;
}
