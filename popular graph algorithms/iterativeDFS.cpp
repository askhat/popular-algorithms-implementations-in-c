#include <bits/stdc++.h>
using namespace std;

unordered_map<int, vector<int> > graph;
unordered_map<int, bool> used, visited;

void depthFirstRec(int node) {
	used[node] = true;
	cout << node << " ";
	for (const auto& to : graph[node]) {
		if (!used[to]) {
			depthFirstRec(to);
		}
	}
}

void depthFirstIterative(int node) {
	stack<int> toVisit;
	toVisit.push(node);


	while (!toVisit.empty()) {
		auto cur = toVisit.top();
		toVisit.pop();
		if (!visited[cur]) {
			cout << cur << " ";
			visited[cur] = true;
		}
		for (const auto& to : graph[cur]) {
			if (!visited[to]) {
				toVisit.push(to);
			}
		}
	}
}


int main() {
	freopen("in", "r", stdin);
	freopen("out", "w", stdout);
	int n, m, from, to;
	cin >> n >> m;
	for (int index = 0; index < m; ++index) {
		cin >> from >> to;
		graph[from].push_back(to);
	}
	depthFirstRec(1);
	cout << endl;
	depthFirstIterative(1);
	return 0;
}
