#include <bits/stdc++.h>
using namespace std;

unordered_map<int, vector<pair<int, int> > > graph;

void dijkstra(int start, int n) {
	vector<int> dist(n + 1, INT_MAX);
	dist[start] = 0;
	set<pair<int, int > > toVisit;
	toVisit.insert({dist[start], start});

	while (!toVisit.empty()) {
		auto cur = toVisit.begin()->second;
		toVisit.erase(toVisit.begin());

		for (const auto& tuple : graph[cur]) {
			auto to = tuple.first, weight = tuple.second;
			if (dist[to] > dist[cur] + weight) {
				toVisit.erase({dist[to], to});
				dist[to] = dist[cur] + weight;
				toVisit.insert({dist[to], to});
			}
		}
	}

	for (int node = 1; node <= n; node++) {
		cout << start << " -> " << node << " = " << dist[node] << endl;
	}
}


void dijkstra_pq(int start, int n) {
	vector<int> dist(n + 1, INF);
	vector<bool> used(n + 1, false);

	dist[start] = 0;
	priority_queue<pair<int,int>, vector<pair<int,int>>, greater<pair<int,int>>> pq;
	pq.push({dist[start], start});
	while (!pq.empty()) {
		auto cur = pq.top().second;
		pq.pop();
		if (used[cur]) continue;
		used[cur] = true;
		for (auto& it : g[cur]) {
			int to = it.F;
			int w = it.S;
			if (dist[to] > dist[cur] + w) {
				dist[to] = dist[cur] + w;
				pq.push({dist[to], to});
			}
		}
	}
	for (int i = 1; i <= n; i++) {
		cout << i << " -> " << dist[i] << endl;
	}
}



int main() {
	freopen("in", "r", stdin);
	freopen("out", "w", stdout);
	int n, m, from, to, weight;
	cin >> n >> m;
	for (int index = 0; index < m; ++index) {
		cin >> from >> to >> weight;
		graph[from].push_back({to, weight});
	}
	dijkstra(1, n);

	return 0;
}
