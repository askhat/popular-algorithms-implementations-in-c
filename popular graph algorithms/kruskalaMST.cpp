#include <iostream>
#include <cstdio>
#include <vector>
#include <map>
#include <unordered_map>
#include <algorithm>

using namespace std;

struct Edge {
	int from, to, dist;
};

struct Node {
	int rank;
	Edge data;
	Node* parent;
};

vector<Edge> result;
map<Edge, Node*> curMap;

void createSet(Edge curEdge) {
	Node* temp = new Node;
	temp -> rank = 0;
	temp -> parent = temp;
	temp -> data = curEdge;

	curMap[curEdge] = temp;
}

Node* getSetParent(Node* cur) {
	Node* curParent = cur -> parent;
	if(curParent == cur) {
		return cur;
	}
	cur -> parent = getSetParent(cur -> parent);

	return cur -> parent;
}


bool createUnion(Edge edge1, Edge edge2) {
	Node* node1 = curMap[edge1];
	Node* node2 = curMap[edge2];

	Node* parent1 = getSetParent(node1), *parent2 = getSetParent(node2);

	if(parent1 == parent2) return false;

	if(parent1 -> rank >= parent2 -> rank) {
		if(parent1 -> rank == parent2 -> rank) {
			parent1 -> rank += 1;
		}
		parent2 -> parent = parent1;
	} else {
		parent1 -> parent = parent2;
	}

	return true;
}


bool edgeComparator(Edge firstEdge, Edge secondEdge) {
	return firstEdge.dist <= secondEdge.dist;
}

int main() {
	freopen("in", "r", stdin);
	freopen("out", "w", stdout);
	int n, m, x, y, w;
	cin >> n >> m;
	vector<Edge> allEdges;
	for(int i = 0; i < m; i++) {
		cin >> x >> y >> w;
		Edge curEdge;
		curEdge.from = x;
		curEdge.to = y;
		curEdge.dist = w;

		allEdges.push_back(curEdge);
	}

	sort(allEdges.begin(), allEdges.end(), edgeComparator);

	for(int i = 0; i < allEdges.size(); i++) {
		createSet(allEdges[i]);
	}

	if(!allEdges.empty()) {
		Edge mainEdge = allEdges[0];
		result.push_back(allEdges[0]);

		for(int i = 1; i < allEdges.size(); i++) {
			if(createUnion(mainEdge, allEdges[i]) {
				result.push_back(allEdges[i]);
			}
		}
	}

	for(int i = 0; i < result.size(); i++) {
		cout << result[i].from << " " << result[i].to << " " << result[i].dist << endl;
	}








	return 0;
}
