#include <bits/stdc++.h>

using namespace std;

unordered_map<int, vector<int> > graph;

bool depthFirst(int node,vector<int>& color) {
	color[node] = 1;

	vector<int> adj = graph[node];
	for(int i = 0; i < adj.size(); i++) {
		int to = adj[i];
		if(color[to] == 1) {
			return true;
		}
		// If "to" is not processed and there is a back
        // edge in subtree rooted with v

		//note: what is the back edge?
		// if V has already been visited:
		//if V is an ancestor of U, then edge (U, V) is a BACK EDGE
		if(color[to] == 0 && depthFirst(to, color)) {
			return true;
		}
	}
	color[node] = 2;
	return false;
}

bool hasCycle(int n) {
	vector<int> color(n + 1);
	//0 white, 1 gray, 2 black
	bool result = false;
	for(int i = 1; i <= n; i++) {
		if(color[i] == 0) {
			if(depthFirst(i, color)) {
				result = true;
				break;
			}
		}
	}
	return result;
}


int main() {
	freopen("in", "r", stdin);
	freopen("out", "w", stdout);
	int n, m, x, y;
	cin >> n >> m;
	for(int i = 0; i < m; i++) {
		cin >> x >> y;
		graph[x].push_back(y);
	}

	bool check = hasCycle(n);
	if(check) {
		cout << "HAS CYCLE!";
	} else {
		cout << "NO CYCLE!";
	}


	return 0;
}
