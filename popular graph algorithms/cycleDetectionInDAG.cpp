#include <bits/stdc++.h>

using namespace std;

// Use the following approach:
// consider we have three colors, and each vertex should be painted with one of these colors.
// "White color" means that the vertex hasn't been visited yet.
// "Gray" means that we've visited the vertex but haven't visited all vertices in its subtree.
// "Black" means we've visited all vertices in subtree and left the vertex.
// So, initially all vertices are white. When we visit the vertex, we should paint it gray.
// When we leave the vertex (that is we are at the end of the dfs() function,
// after going throung all edges from the vertex), we should paint it black.
// If you use that approach, you just need to change dfs() a little bit.
// Assume we are going to walk through the edge v->u. If u is white, go there.
// If u is black, don't do anything. If u is gray, you've found the cycle because you haven't left u
// yet (it's gray, not black), but you come there one more time after walking throung some path.
//To keep vertices' colors replace boolean array with char or integer array and just use values in range [0..2].

class GraphCycle {
public:
	bool hasCycle(int numberOfNodes, const vector<pair<int, int> >& edges) {
		constructGraph(edges);
		vector<int> colors(numberOfNodes + 1, 0);
		bool hasCycle = false;
		for (int node = 1; node <= numberOfNodes; node++) {
			if (colors[node] == 0 && !hasCycle) {
				depthFirst(node, colors, &hasCycle);
			}
		}
		return hasCycle;
	}
private:
	unordered_map<int, vector<int> > graph;
	void constructGraph(const vector<pair<int, int> >& edges) {
		for (const auto& edge : edges) {
			graph[edge.first].push_back(edge.second);
		}
	}

	void depthFirst(int node, vector<int>& colors, bool* hasCycle) {
		colors[node] = 1;
		auto adj = graph[node];

		for (const auto& to : adj) {
			if (colors[to] == 1) {
				*hasCycle = true;
				return;
			}
			if (colors[to] == 0 && !*hasCycle) {
				depthFirst(to, colors, hasCycle);
			}
		}
		colors[node] = 2;
	}
};


int main() {
	freopen("in", "r", stdin);
	freopen("out", "w", stdout);
	int n, m, from, to;
	cin >> n >> m;
	vector<pair<int, int> > edges(m);
	for (int index = 0; index < m; index++) {
		cin >> edges[index].first >> edges[index].second;
	}
	GraphCycle g;
	if (g.hasCycle(n, edges)) {
		cout << "Graph has a cycle!" << endl;
	} else {
		cout << "Graph does not have a cycle!" << endl;
	}


	return 0;
}
