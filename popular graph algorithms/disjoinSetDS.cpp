#include <bits/stdc++.h>
using namespace std;

struct Node {
    int data, rank;
    shared_ptr<Node> parent;
    Node(int x) : rank(0), data(x), parent(this) {}
};

unordered_map<int, shared_ptr<Node>> getNode;

void createSet(int data) {
    shared_ptr<Node> tmp(new Node(data));
    getNode[data] = tmp;
}

shared_ptr<Node> getParent(const shared_ptr<Node>& node) {
    auto parent = node->parent;
    if (parent == node) {
        return parent;
    }
    node->parent = getParent(node->parent);
    return node->parent;
}

int getRepresentative(int data) {
    auto node = getNode[data];
    return getParent(node)->data;
}

void unionSets(int data1, int data2) {
    shared_ptr<Node> p1 = getParent(getNode[data1]), p2 = getParent(getNode[data2]);
    if (p1 == p2) {
        return;
    }
    if (p1->rank >= p2->rank) {
        p2->parent = p1;
        p1->rank++;
    } else {
        p1->parent = p2;
    }
}


int main(int argc, char const *argv[]) {
    ios_base::sync_with_stdio(false); cin.tie(NULL);
    #ifndef ONLINE_JUDGE
    freopen("in", "r", stdin); freopen("out", "w", stdout);
    #endif
    int n;
    cin >> n;
    for (size_t i = 0; i < n; i++) {
        createSet(i);
    }
    unionSets(1, 2);
    unionSets(9, 0);
    unionSets(9, 6);
    unionSets(8, 3);

    for (size_t i = 0; i < n; i++) {
        cout << getRepresentative(i) << endl;
    }




    return 0;
}

