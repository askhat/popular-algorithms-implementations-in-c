#include <bits/stdc++.h>

using namespace std;

class Solution {
public:
	void topSort(unordered_map<int, vector<int> >& graph, int numberOfNodes) {
		stack<int> order;
		for (int node = 1; node <= numberOfNodes; node++) {
			if (used.find(node) == used.end()) {
				depthFirst(order, node, graph);
			}
		}
		//show
		while (!order.empty()) {
			cout << order.top() << " ";
			order.pop();
		}
	}
private:
	unordered_map<int, bool> used;
	void show(stack<int>& order) {
		cout << "Topological sort order is " << endl;
		while (!order.empty()) {
			cout << order.top() << " ";
			order.pop();
		}
	}
	void depthFirst(stack<int>& order, int node, unordered_map<int, vector<int> >& graph) {
		used[node] = true;
		auto adj = graph[node];
		for (const auto& to : adj) {
			if (used.find(to) == used.end()) {
				depthFirst(order, to, graph);
			}
		}
		order.push(node);
	}
};


int main() {
	freopen("in", "r", stdin);
	freopen("out", "w", stdout);
	int n, m, from, to;
	cin >> n >> m;
	unordered_map<int, vector<int> > graph;
	for (int index = 0; index < m; index++) {
		cin >> from >> to;
		graph[from].push_back(to);
	}
	Solution sol;
	sol.topSort(graph, n);


	return 0;
}
